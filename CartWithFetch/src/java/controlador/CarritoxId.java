package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Producto;
import modelo.ProductoDAO;

public class CarritoxId extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        Gson convertir = new Gson();

        String filtro = request.getParameter("filtroId");

        if (filtro != null && filtro.length() > 0) {
            //The specified key is stored in a variable.
            String elnombre = filtro;
            //We look for the specific row through the value entered by the user and store it in a variable. 
            ArrayList<Producto> valorFinal = ProductoDAO.ProductoxIndice(elnombre);
            //This condition evaluates if the variable contains information.
            if (valorFinal != null) {
                //It converts the value to json format and displays it to the navigation website.
                out.print(convertir.toJson(valorFinal));
            }

        } else {
            //This variable stores all the rows in the table.
            ArrayList<Producto> productos = ProductoDAO.Listado();
            //It evaluates if the variable is not null. 
            if (productos != null) {
                //The whole table is converted to JSON and stored in a variable.
                String resultado = convertir.toJson(productos);
                //We send the result to the client
                out.println("" + resultado);
            }
        }
    }

    
}
