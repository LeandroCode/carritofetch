package controlador;

import com.google.gson.Gson;
import configuracion.Conexion;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ControlClientePost extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       response.setContentType("text/html;charset=UTF-8");
        //This variable stores the reading of the data entered by the user.
        BufferedReader reader = request.getReader();
        //It constructs a gson object.
        Gson convertir = new Gson();
        
        TreeMap<String, String> clie = convertir.fromJson(reader, TreeMap.class);
        //It stores the values obtained from the database in a string.
        String usuario = clie.get("usu");
        String documento = clie.get("dni");
        String email = clie.get("email");
        String contra = clie.get("contra");
         
         System.out.println("Usuario: "+usuario);
         System.out.println("Documento: "+documento);
         System.out.println("Email: "+email);
         System.out.println("Contrasenia: "+contra);

        try {
            //We obtain the connection through the method and assign it to a variable.
            Connection con = Conexion.getConnection();
            //It generates the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = con.prepareStatement("INSERT into cliente (idCliente,Usuario,DNI,Email,Password)"
                    + " VALUES (null,?,?,?,?)");
            //It sets each variable submitted by the user from the website to the database. 
            sentencia.setString(1, usuario);
            sentencia.setString(2, documento);
            sentencia.setString(3, email);
            sentencia.setString(4, contra);

            //Execute the query and then close the connection.
            sentencia.executeUpdate();
            sentencia.close();
            con.close();
            //Connection error
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());

        }
    }

   

}
