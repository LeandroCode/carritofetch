package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Cliente;

public class ConsultarClienteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        System.out.println("entro por aca");
        Cliente cli = (Cliente) session.getAttribute("usuario");

        Gson convertirlo = new Gson();

        out.print(convertirlo.toJson(cli));
        System.out.println("Objeto:" + convertirlo.toJson(cli));

        //           request.getRequestDispatcher("carrito.html").forward(request, response);
    }

}
