package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Carrito;
import modelo.Producto;
import modelo.ProductoDAO;

@WebServlet(name = "ControlCarrito", urlPatterns = "/ControlCarrito")
public class ControlCarrito extends HttpServlet {

    List<Carrito> listaCarrito = new ArrayList<>();
    Producto p=new Producto();
    
    int item;
    double totalPagar = 0.0;
    int cantidad = 1;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int pos = 0;
        int cant = 1;
        PrintWriter out = response.getWriter();

        Gson convertir = new Gson();

        ServletInputStream filtroId = request.getInputStream();

        String filtroJson = convertir.fromJson(new InputStreamReader(filtroId), String.class);

        System.out.println("filtroId: " + filtroJson);

        p = ProductoDAO.BuscarXindice(Integer.parseInt(filtroJson));
        if (listaCarrito.size() > 0) {
            for (int i = 0; i < listaCarrito.size(); i++) {
                if (Integer.parseInt(filtroJson) == listaCarrito.get(i).getIdProducto()) {
                    pos = i;
                }
            }
            if (Integer.parseInt(filtroJson) == listaCarrito.get(pos).getIdProducto()) {

                cant = listaCarrito.get(pos).getCantidad() + cant;
                double subtotal = listaCarrito.get(pos).getPrecioCompra() * cant;
                listaCarrito.get(pos).setCantidad(cant);
                listaCarrito.get(pos).setSubTotal(subtotal);
            } else {
                item = item + 1;
                Carrito car = new Carrito();
                Carrito.setCantidadItems(item);
                car.setItem(item);
                car.setIdProducto(p.getId());
                car.setCodigodecompra(p.getCodigoCompra());
                car.setNombres(p.getNombres());
                car.setDescripcion(p.getDescripcion());
                car.setFoto(p.getFoto());
                car.setPrecioCompra(p.getPrecio());
                car.setCantidad(cantidad);
                car.setSubTotal(cantidad * p.getPrecio());
                listaCarrito.add(car);
                String resultado = convertir.toJson(car);
                out.println("" + resultado);
            }

        } else {
            item = item + 1;
            Carrito car = new Carrito();
            Carrito.setCantidadItems(item);
            car.setItem(item);
            car.setIdProducto(p.getId());
            car.setCodigodecompra(p.getCodigoCompra());
            car.setNombres(p.getNombres());
            car.setDescripcion(p.getDescripcion());
            car.setFoto(p.getFoto());
            car.setPrecioCompra(p.getPrecio());
            car.setCantidad(cantidad);
            car.setSubTotal(cantidad * p.getPrecio());
            listaCarrito.add(car);
            String resultado = convertir.toJson(car);
            out.println("" + resultado);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        Gson convertir = new Gson();
        String resultado = convertir.toJson(listaCarrito);
        out.println("" + resultado);
        System.out.println("resultado: " + resultado);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        
        String filtroErased = req.getParameter("q");
        int idProducto = Integer.parseInt(filtroErased);
        for(int i = 0; i < listaCarrito.size(); i++){
            if(listaCarrito.get(i).getItem()==idProducto){
                listaCarrito.remove(i);
            }else{
 
            }
        }
        item = item - 1;
        Carrito.setCantidadItems(item);
    }
    
    
}
