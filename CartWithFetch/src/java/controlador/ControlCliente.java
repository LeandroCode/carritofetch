package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Cliente;
import modelo.ClienteDAO;

public class ControlCliente extends HttpServlet {

    ClienteDAO dao = new ClienteDAO();
    Cliente clie = new Cliente();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        Gson convertirlo = new Gson();

        String filtro = request.getParameter("filtro");

        Cliente filtroJsonn = convertirlo.fromJson(filtro, Cliente.class);

        clie = dao.Validar(filtroJsonn);

        if (clie != null) {

            Cliente cli = new Cliente(clie.getUsuario(), clie.getEmail(), clie.getPass());
            HttpSession session = request.getSession();
            session.setAttribute("usuario", cli);

            request.getRequestDispatcher("ConsultarClienteServlet").forward(request, response);

        } else {

            out.print(convertirlo.toJson(clie));

        }
    }

}
