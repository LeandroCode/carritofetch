package modelo;

public class Cliente {
    
    String usuario;
    String dni;
    String email;
    String pass;

    public Cliente() {
    }

    public Cliente(String usuario, String email, String pass) {
        this.usuario = usuario;
        this.email = email;
        this.pass = pass;
    }
    
        
    public Cliente(String usuario, String dni, String email, String pass) {
        this.usuario = usuario;
        this.dni = dni;
        this.email = email;
        this.pass = pass;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    
}
