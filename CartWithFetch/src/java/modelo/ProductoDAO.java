package modelo;

import configuracion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;





public class ProductoDAO extends Conexion{
    public static ArrayList<Producto> Listado(){
        
        ArrayList<Producto> productos=new ArrayList<>();
        Connection con;
        
        try {
            con = Conexion.getConnection();
            PreparedStatement consulta=con.prepareStatement("Select * from producto");
            ResultSet resultado = consulta.executeQuery();
            while(resultado.next()){
                Producto prod=new Producto();
                prod.setId(resultado.getInt("idProducto"));
                prod.setCodigoCompra(resultado.getString("codcompra"));
                prod.setNombres(resultado.getString("nombre"));
                prod.setFoto(resultado.getString("foto"));
                prod.setDescripcion(resultado.getString("descripcion"));
                prod.setPrecio(resultado.getDouble("precio"));
                prod.setStock(resultado.getInt("stock"));
                productos.add(prod);
            }
            resultado.close();
            consulta.close();
            con.close();
        } catch (Exception e) {
           System.err.println("SQLException: " + e.getMessage());
           return null;
        }
        
        
        return productos;
    }
    
    public static Producto BuscarXindice(int id){
    
            Producto prod=new Producto();
            Connection con;
            
        try {
            //We obtain the connection through the method and assign it to a variable.
            con = Conexion.getConnection();
            //It prepares the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = con.prepareStatement("Select * from producto where idProducto =" + id);
            //It executes the query and stores it in a variable called result.
            ResultSet resultado = sentencia.executeQuery();
            //Condition used to ask if there is more data in the table
            //so The next method moves the cursor to the next row.
            while(resultado.next()){
                
                prod.setId(resultado.getInt("idProducto"));
                prod.setCodigoCompra(resultado.getString("codcompra"));
                prod.setNombres(resultado.getString("nombre"));
                prod.setFoto(resultado.getString("foto"));
                prod.setDescripcion(resultado.getString("descripcion"));
                prod.setPrecio(resultado.getDouble("precio"));
                prod.setStock(resultado.getInt("stock"));
            }
            //Execute the query and then close the connection.
            resultado.close();
            sentencia.close();
            con.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());
           return null;
        }
        //It contains and returns the specific row from table 'autos'.
         return prod;
    }

    public static ArrayList<Producto> ProductoxIndice(String elnombre){
    
            ArrayList<Producto> listaXId = new ArrayList();
            Connection con;
            
        try {
            //We obtain the connection through the method and assign it to a variable.
            con = Conexion.getConnection();
            
            String sql="Select * from producto where nombre like '%" + elnombre + "%' ";
            //It prepares the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = con.prepareStatement(sql);
            //It executes the query and stores it in a variable called result.
            ResultSet resultado = sentencia.executeQuery();
            //Condition used to ask if there is more data in the table
            //so The next method moves the cursor to the next row.
            while(resultado.next()){
                
                Producto prod=new Producto();
                prod.setId(resultado.getInt("idProducto"));
                prod.setCodigoCompra(resultado.getString("codcompra"));
                prod.setNombres(resultado.getString("nombre"));
                prod.setFoto(resultado.getString("foto"));
                prod.setDescripcion(resultado.getString("descripcion"));
                prod.setPrecio(resultado.getDouble("precio"));
                prod.setStock(resultado.getInt("stock"));
                listaXId.add(prod);
               
            }
            //Execute the query and then close the connection.
            resultado.close();
            sentencia.close();
            con.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());
           return null;
        }
        //It contains and returns the specific row from table 'autos'.
         return listaXId;
    }
    
}
