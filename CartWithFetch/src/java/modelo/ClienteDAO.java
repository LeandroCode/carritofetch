package modelo;

import configuracion.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class ClienteDAO implements Validar{

       
    @Override
    public Cliente Validar(Cliente cli) {
        
        String sql="Select * from cliente where Usuario=? and Email=? and Password=?";
        Connection con;
        int r=0;
        try {
            con = Conexion.getConnection();
            PreparedStatement ps=con.prepareStatement(sql);
            
            ps.setString(1, cli.getUsuario());
            ps.setString(2, cli.getEmail());
            ps.setString(3, cli.getPass());
            ResultSet resultado = ps.executeQuery();
            while(resultado.next()){
                r=r+1;
                cli.setUsuario(resultado.getString("Usuario"));
                cli.setEmail(resultado.getString("Email"));
                cli.setPass(resultado.getString("Password"));
                       
            }
            if(r==1){
                return cli;
                
            }else{
                return null;
            }
      
        } catch (Exception e) {
            System.err.println("SQLException: " + e.getMessage());
           return null;
        }
    }
}
