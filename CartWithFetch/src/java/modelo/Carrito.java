package modelo;

public class Carrito {
    int item;
    int idProducto;
    String codigodecompra;
    String nombres;
    String descripcion;
    String foto;
    double precioCompra;
    int cantidad;
    double subTotal;
    private static int cantidadItems = 0;
    public Carrito() {
    }

   
    public Carrito(int item, int idProducto, String codigodecompra, String nombres, String descripcion, String foto, double precioCompra, int cantidad, double subTotal) {
        this.item = item;
        this.idProducto = idProducto;
        this.codigodecompra = codigodecompra;
        this.nombres = nombres;
        this.descripcion = descripcion;
        this.foto = foto;
        this.precioCompra = precioCompra;
        this.cantidad = cantidad;
        this.subTotal = subTotal;
    }
    
     public static int getCantidadItems() {
        return cantidadItems;
    }

    public static void setCantidadItems(int cantidadItems) {
        Carrito.cantidadItems = cantidadItems;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public String getCodigodecompra() {
        return codigodecompra;
    }

    public void setCodigodecompra(String codigodecompra) {
        this.codigodecompra = codigodecompra;
    }
      
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }
}