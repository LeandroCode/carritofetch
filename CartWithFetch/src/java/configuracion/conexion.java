package configuracion;

import java.sql.Connection;
import java.sql.DriverManager;

public abstract class Conexion {
    
    public Conexion() {
    }
    
    public static Connection getConnection() throws Exception{
        
        //Establece el nombre del driver a utilizar
        String dbDriver = "com.mysql.jdbc.Driver";
        
        //Establece la conexion a utilizar contra la base de datos
        String dbConexion = "jdbc:mysql://localhost/bdcarritocompras";
        
        //Establece el usuario de la base de datos
        String dbUsuario = "root";
        
        //Establece la contraseña de la base de datos
        String dbContraseña = "";
        
        //Establece el driver de conexion
        Class.forName(dbDriver).newInstance();
        
        //Retorna la conexion
        return DriverManager.getConnection(dbConexion,dbUsuario,dbContraseña);
    }
}
