function consultCart() {

    url = "ControlCarrito";

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(dataCarr => traerCarrito(dataCarr)
            );

}
function traerCarrito(datosCarr) {

    let tablaCarr = datosCarr.map(eldato =>
            `
               <tr>
                    <td>${eldato.item}</td>
                    <td>${eldato.nombres}</td>
                    <td><img src="${eldato.foto}" width="100" height="80">
                        ${eldato.descripcion}
                    </td>
                    <td>${eldato.precioCompra}</td>
                    <td>${eldato.cantidad}</td>
                    <td>${eldato.subTotal}</td>
                    <td>
                        <a href="#" onclick="deleteId(${eldato.item})" ><i class="fas fa-trash-alt"></i></a>
                    </td>
               </tr>
            `
    ).join('');

    document.getElementById("result").innerHTML = tablaCarr;

}


function deleteId(idp) {
    swal({
        title: "Estas Seguro?",
        text: "Una vez eliminado, no podras recuperar este archivo!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
                    url = "ControlCarrito?&";

                    fetch(url + "q=" + idp, {
                        method: 'DELETE',
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8"
                        }
                    })
                            .then(data => {
                                consultCart(data);
                                subTotal();

                            });
                    swal("Poof! Tu archivo ha sido borrado!", {
                        icon: "success",
                    });
                } else {
                    swal("Tu archivo esta a salvo!");
                }
            });

}


function subTotal() {
    url = "ControlCarrito";
    let miSubtotal = 0;
    let miTotal = 0;

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(elementos => {
                if (elementos.length === 0) {
                    document.querySelector("#subtotal").value = 0;
                    document.querySelector("#total").value = 0;
                } else {
                    elementos.map(elelemento => document.querySelector("#subtotal").value = miSubtotal += elelemento.subTotal)
                    elementos.map(elelemento => document.querySelector("#total").value = miTotal += elelemento.subTotal)
                }

            }

            )


}

function ValidarUsuario() {

    elobjeto = {};

    elobjeto.usuario = document.querySelector('#txtuser').value;
    elobjeto.email = document.querySelector('#txtema').value;
    elobjeto.pass = document.querySelector('#txtpass').value;

    if (elobjeto.usuario.length === 0 && elobjeto.email.length === 0 && elobjeto.pass.length === 0) {
        jQuery(function () {
            swal({icon: "warning",
                title: "No Puedes Comprar!",
                text: "...Inicia Sesion Por Favor!"

            });
        });
        return;
    } else {
        location.href = "detalle.html";
    }

}

function refreshSession() {

    url = "ConsultarClienteServlet";

    elobjeto = {};

    elobjeto.usuario = document.querySelector('#txtuser').value;
    elobjeto.email = document.querySelector('#txtema').value;
    elobjeto.pass = document.querySelector('#txtpass').value;


    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(usu => {
                if (usu.usuario.length !== 0 && usu.email.length !== 0 && usu.pass.length !== 0){
                    document.getElementById("session").innerHTML = document.querySelector('#txtuser').value = usu.usuario;
                    document.getElementById("sessionOk").innerHTML = document.querySelector('#txtuser').value = usu.usuario;
                    document.getElementById("emailOk").innerHTML = document.querySelector('#txtema').value = usu.email;
                    document.querySelector('#txtpass').value = usu.pass;
                }else{
                    location.href = "detalle.html";
                }


            }
            );

}

function CerrarSesionServlet(){
    
    url = "CerrarSesionServlet";
    
    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/x-www-form-urlencoded;charset=UTF-8"
        }
    })
            .then( data =>   
        window.location = "index.html"
                
    );
    
}


function main() {
    refreshSession();
    consultCart();
    subTotal();
}

main();

