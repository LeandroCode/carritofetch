
function fetchConsult() {

    url = "Catalogo";

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(datos => renderCarrito(datos)
            )

}

function renderCarrito(productos) {

    let tablaProductos = productos.map(productodata =>
            `
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header">
                            <label>${productodata.nombres}</label>
                        </div>
                        <div class="card-body">
                            <i>${productodata.precio}</i>
                            <img src="${productodata.foto}" style="margin-left: 25px;" width="200" height="180">
                        </div>
                        <div class="card-footer text-center">
                            <label>${productodata.descripcion}</label>
                            <div>
                                <a href="#" onclick="addItemToCart(${productodata.id})" class="btn btn-outline-info">Agregar a Carrito</a>
                                <a href="carrito.html" onclick="addProductToCart(${productodata.id})" class="btn btn-danger">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
            `
    ).join('');

    document.getElementById("response").innerHTML = tablaProductos;

}

function addItemToCart(itemId) {

    url = "ControlCarrito";
        
    fetch(url, {
        method: 'POST',
        body: itemId,
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(
                    datos => 
                          document.getElementById("itemCart").innerHTML  = datos.item
                         
            ); 
                   
}

function addProductToCart(addItemId) {

    url = "ControlCarrito";
    fetch(url, {
        method: 'POST',
        body: addItemId,
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(
                    datos =>  console.log(datos)
            );
}

function consultCartItems() {

    url = "Cantidaditems";
    fetch(url, {
        method: 'GET',
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(
                    datos =>
                document.getElementById("itemCart").innerHTML = datos.item

            );
}

function carritoConsult() {

    url = "CarritoxId?&";

    elobjeto = {};

    elobjeto.filtroId = document.querySelector('#texto').value;

    fetch(url + "filtroId=" + elobjeto.filtroId)
            .then(
                    response => response.json()
            )
            .then( data => renderProductos(data)
            
            )
    
}
               
function renderProductos(prods){
    console.log("Productos: " + JSON.stringify(prods));
    let tableProductos = prods.map( elprod =>
                `
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header">
                            <label>${elprod.nombres}</label>
                        </div>
                        <div class="card-body">
                            <i>${elprod.precio}</i>
                            <img src="${elprod.foto}" style="margin-left: 25px;" width="200" height="180">
                        </div>
                        <div class="card-footer text-center">
                            <label>${elprod.descripcion}</label>
                            <div>
                                <a href="#" onclick="addItemToCart(${elprod.id})" class="btn btn-outline-info">Agregar a Carrito</a>
                                <a href="carrito.html" onclick="addProductToCart(${elprod.id})" class="btn btn-danger">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
            `
            ).join('');
               
        document.getElementById("response").innerHTML = tableProductos;
            
}

document.getElementById("elbotonazo").addEventListener("click", function () {
    carritoConsult();
});

function main(){
    fetchConsult();
    consultCartItems();
 
}
    
main();