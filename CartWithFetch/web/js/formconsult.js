function formConsult(datos) {

    url = "ControlCliente?&";

    elobjeto = {};

    elobjeto.usuario = document.querySelector('#txtuser').value;
    elobjeto.email = document.querySelector('#txtema').value;
    elobjeto.pass = document.querySelector('#txtpass').value;

    if (elobjeto.usuario.length === 0 || elobjeto.email.length === 0 || elobjeto.pass.length === 0) {

        jQuery(function () {
            swal({icon: "warning",
                title: "¡Por Favor!",
                text: "Vuelve a Ingresar Usuario Valido"
            }).then(function () {
                $("#modal1").modal({
                    backdrop: 'static',
                    keyboard: false
                });

            });
        });
        return;
    }
    fetch(url + "filtro=" + JSON.stringify(elobjeto), {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(datos => {
                if (datos !== null) {
                    document.getElementById("session").innerHTML = datos.usuario;
                    document.getElementById("sessionOk").innerHTML = datos.usuario;
                    document.getElementById("emailOk").innerHTML = datos.email;
                }else{
                    jQuery(function () {
                        swal({icon: "warning",
                            title: "¡No Se Encuentra Ese Registro!",
                            text: "Ingresa uno Valido!"
                        }).then(function () {
                            $("#modal1").modal({
                                backdrop: 'static',
                                keyboard: false
                            });
                        });
                    });
                }

            });

}
function fetchInsert(data) {

    data.preventDefault();

    url = "ControlClientePost?&";

    elobjeto = {};

    elobjeto.usu = document.querySelector('#txtusu').value;

    if (elobjeto.usu.length === 0) {
        jQuery(function () {
            swal({icon: "warning",
                title: "¡Espera!",
                text: "El Usuario no Puede Estar Vacio!"

            }).then(function () {
                $("#modal1").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        });
        return;
    }

    elobjeto.dni = document.querySelector('#txtdoc').value;

    if (elobjeto.dni.length === 0) {
        jQuery(function () {
            swal({icon: "warning",
                title: "¡Espera!",
                text: "El Documento no Puede Estar Vacio!"
            }).then(function () {
                $("#modal1").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        });
        return;
    }

    elobjeto.email = document.querySelector('#txtemail').value;

    if (elobjeto.email.length === 0) {
        jQuery(function () {
            swal({icon: "warning",
                title: "¡Espera!",
                text: "El Correo no Puede Estar Vacio!"
            }).then(function () {
                $("#modal1").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        });
        return;
    }

    elobjeto.contra = document.querySelector('#txtcontra').value;

    if (elobjeto.contra.length < 6) {
        jQuery(function () {
            swal({icon: "warning",
                title: "¡Espera!",
                text: "El Password no Puede Estar Vacio!"
            }).then(function () {
                $("#modal1").modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
        });
        return;
    }


    fetch(url, {
        method: 'POST',
        body: JSON.stringify(elobjeto),
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(data => {
                if (data) {
                    jQuery(function () {
                        swal({icon: "success",
                            title: "¡Bien Hecho!",
                            text: "Usuario Registrado con Exito!"
                        }).then(function () {


                        });

                    });
                }

            })


}


function main() {
    document.addEventListener("DOMContentLoaded", function () {
        document.getElementById("botonInsert").addEventListener('click', fetchInsert);
    });

    document.getElementById("elboton").addEventListener("click", function () {
        formConsult();
    });

}

main();


