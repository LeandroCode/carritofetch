function consultCart() {

    url = "ControlCarrito";

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(dataCarr => {
                if (dataCarr.length !== 0) {
                    traerCarrito(dataCarr)
                    
                } else {
                    jQuery(function () {
                        swal({type: "success",
                            title: "¡No Puedes Entrar!",
                            text: "Vuelve Cuando tengas Productos"
                        }).then(function () {
                            window.location = "index.html";
                        });
                    });
                }

            })


}
function traerCarrito(datosCarr) {

    let tablaCarr = datosCarr.map(eldato =>
            `
               <tr>
                    <td>${eldato.codigodecompra}</td>
                    <td>
                        <img src="${eldato.foto}" width="100" height="80">
                    </td>
                    <td>${eldato.cantidad}</td>
                    <td>${eldato.precioCompra}</td>
               </tr>
            `
    ).join('');

    document.getElementById("result").innerHTML = tablaCarr;

}
function total() {
    url = "ControlCarrito";
    let miTotal = 0;

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(elementos => {
                if (elementos.length === 0) {
                    document.querySelector("#total").value = 0;
                } else {
                    elementos.map(elelemento => document.querySelector("#total").value = miTotal += elelemento.subTotal)
                }

            }

            )


}

function consultUsuario() {

    url = "ConsultarClienteServlet";

    elobjeto = {};
    
    elobjeto.usuario; 

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(usu => document.getElementById("sessionIn").innerHTML = usu.usuario);


}

function VolverCarrito(){
    
    location.href = "carrito.html";   
          
}

function main() {
    consultUsuario();
    consultCart();
    total();
}

main();
