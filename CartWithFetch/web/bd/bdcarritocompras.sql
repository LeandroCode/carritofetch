-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-11-2020 a las 21:20:27
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdcarritocompras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) UNSIGNED NOT NULL,
  `Usuario` varchar(255) NOT NULL,
  `DNI` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `Usuario`, `DNI`, `Email`, `Password`) VALUES
(1, 'Augusto Hernandez', '26351954', 'augusto54@hotmail.com', 'augusto123'),
(2, 'Leonel Miranda', '32651425', 'leo_25@gmail.com', 'leonel123'),
(3, 'Angela Gabrielli', '35486579', 'angela_79@gmail.com', 'angela123'),
(6, 'Marcelo', '22564381', 'marce88@gmail.com', 'marce88'),
(8, 'Uriel Dominguez', '45321685', 'urido@gmail.com', 'uri87'),
(9, 'Leo Gutierrez', '32456147', 'leito@gmail.com', 'leito1982'),
(12, 'pepe', '12345678', 'pepito@pepito.com', 'pepito123'),
(32, 'Joaquin', '26453145', 'joaco@gmail.com', 'joa88');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `idCompras` int(11) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL,
  `idPago` int(11) UNSIGNED NOT NULL,
  `fechaCompras` varchar(11) NOT NULL,
  `monto` double NOT NULL,
  `estado` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compras`
--

CREATE TABLE `detalle_compras` (
  `idDetalle` int(10) UNSIGNED NOT NULL,
  `idProducto` int(11) UNSIGNED NOT NULL,
  `idCompras` int(11) UNSIGNED NOT NULL,
  `cantidad` int(11) UNSIGNED NOT NULL,
  `precioCompra` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE `pago` (
  `idPago` int(11) UNSIGNED NOT NULL,
  `monto` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) UNSIGNED NOT NULL,
  `codcompra` varchar(10) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precio` double NOT NULL,
  `stock` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `codcompra`, `nombre`, `foto`, `descripcion`, `precio`, `stock`) VALUES
(1, 'C0010', 'Laptop Silver - Intel Core i5', 'https://images-na.ssl-images-amazon.com/images/I/91J1PFs3y4L._AC_SL1500_.jpg', 'Full HD Touch-screen 13.3-Inch', 700, 50),
(2, 'C0011', 'Celular Huawei', 'https://www.notebookcheck.org/fileadmin/Notebooks/Huawei/P40/p40_silver.jpg', 'Memoria Interna - 32GB RAM 16GB ', 200, 50),
(3, 'C0012', 'Monitor Lenovo Pc', 'https://www.lenovo.com/medias/61ABMAT1-500-1.png?context=bWFzdGVyfHJvb3R8MTkxNjUzfGltYWdlL3BuZ3xoYjYvaDYzLzEwODI4NDQwMTc0NjIyLnBuZ3wzZjgzMzc3NTIyZmIzNmQzYjNjYTc3NTM1NGMxYWMyYzZjYjRlZjYzOTMwZDJhMmY1YTM4YjI2NGQxNzE3ZGRk', 'Lenovo Led - 22 Pulgadas', 250, 50),
(4, 'C0013', 'Auriculares Wireless - Beats ', 'https://static.sscontent.com/prodimg/thumb/400/400/products/124/v701301_prozis_silentia-day-wireless-noise-cancelling-headphones_single-size_no-code_main.jpg', 'Wireless Color Negro', 150, 50),
(5, 'C0014', 'Pc de Escritorio', 'https://www.onubaelectronica.es/wp-content/uploads/elementor/thumbs/pc-ooywp68nqkg13ezae954javkt59mggs5rdeg8udcp4.jpg', 'Dell Optiplex 3050 I5 7500 8GB', 600, 50),
(6, 'C0015', 'Mouse Logitech', 'https://pardohogar.vteximg.com.br/arquivos/ids/163809-1280-852/mouse.jpg?v=637311099006070000', 'Mouse Inalambrico M187 Negro', 500, 50);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`idCompras`);

--
-- Indices de la tabla `detalle_compras`
--
ALTER TABLE `detalle_compras`
  ADD PRIMARY KEY (`idDetalle`);

--
-- Indices de la tabla `pago`
--
ALTER TABLE `pago`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `idCompras` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `detalle_compras`
--
ALTER TABLE `detalle_compras`
  MODIFY `idDetalle` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pago`
--
ALTER TABLE `pago`
  MODIFY `idPago` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
