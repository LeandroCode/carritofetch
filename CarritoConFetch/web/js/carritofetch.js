
function fetchConsult(datos) {

    url = "Catalogo";

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(datos => renderCarrito(datos)
            )

}

function renderCarrito(productos) {

    let tablaProductos = productos.map(productodata =>
            `
                <div class="col-sm-4">
                    <div class="card">
                        <div class="card-header">
                            <label>${productodata.nombres}</label>
                        </div>
                        <div class="card-body">
                            <i>${productodata.precio}</i>
                            <img src="${productodata.foto}" width="200" height="180">
                        </div>
                        <div class="card-footer text-center">
                            <label>${productodata.descripcion}</label>
                            <div>
                                <a href="#" onclick="addItemToCart(${productodata.id})" class="btn btn-outline-info">Agregar a Carrito</a>
                                <a href="#" class="btn btn-danger">Comprar</a>
                            </div>
                        </div>
                    </div>
                </div>
            `
    ).join('');

    document.getElementById("response").innerHTML = tablaProductos;

}

function addItemToCart(itemId) {

    url = "ControlCarrito";
    fetch(url, {
        method: 'POST',
        body: itemId,
        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(
                    datos =>
                document.getElementById("itemCart").innerHTML = datos.item

            )
}

document.getElementById("botonConsulta").addEventListener("click", function () {
    fetchConsult();
});

function consultCart() {

    url = "ControlCarrito";

    fetch(url, {
        method: 'GET',

        headers: {
            "Content-type": "application/json"
        }
    })
            .then(
                    response => response.json()
            )
            .then(dataCart => 
                    window.onload = function (){
                        document.getElementById("result").innerHTML = dataCart;
                    } 
                     
            );

}
     
document.getElementById("consultarCarrito").addEventListener("click", function () {
    consultCart();
});
