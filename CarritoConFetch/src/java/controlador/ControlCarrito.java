package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Carrito;
import modelo.Producto;
import modelo.ProductoDAO;

@WebServlet(name= "ControlCarrito", urlPatterns="/ControlCarrito")
public class ControlCarrito extends HttpServlet {
        Producto p = new Producto();
        List<Carrito> listaCarrito = new ArrayList<>();
        
        int item;
        double totalPagar = 0.0;
        int cantidad = 1;
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        Gson convertir = new Gson();

        ServletInputStream filtroId = request.getInputStream();
        
        String filtroJson = convertir.fromJson(new InputStreamReader(filtroId), String.class);
        
        System.out.println("filtroId: "+filtroJson);

        p = ProductoDAO.BuscarXindice(Integer.parseInt(filtroJson));
        item = item + 1;
        Carrito car = new Carrito();
        car.setItem(item);
        car.setIdProducto(p.getId());
        car.setNombres(p.getNombres());
        car.setDescripcion(p.getDescripcion());
        car.setPrecioCompra(p.getPrecio());
        car.setCantidad(cantidad);
        
        car.setSubTotal(cantidad * p.getPrecio());
        listaCarrito.add(car);

        if (car != null) {
        
            String resultado = convertir.toJson(car);
            out.println("" + resultado);
            
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        PrintWriter out=resp.getWriter();
        Gson convertir = new Gson();
        String resultado = convertir.toJson(listaCarrito);
        out.println("" + resultado);
        System.out.println("resultado: "+resultado);
    }
    
}
