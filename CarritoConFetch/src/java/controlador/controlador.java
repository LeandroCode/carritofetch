package controlador;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.carrito;
import modelo.producto;
import modelo.productodao;

public class controlador extends HttpServlet {
    
    producto p=new producto();
    List<carrito> listaCarrito=new ArrayList<>(); 
    int item;
    double totalPagar=0.0;
    int cantidad=1;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        PrintWriter out = response.getWriter();

        Gson convertir = new Gson();
       
        String filtro = request.getParameter("filtroId");

                
        if (filtro != null && filtro.length() > 0) {
            String elvalor = filtro;
            //We look for the specific row through the value entered by the user and store it in a variable. 
            p = productodao.BuscarXindice(Integer.parseInt(elvalor));
            item = item + 1;
            carrito car=new carrito();
            car.setItem(item);
            car.setIdProducto(p.getId());
            car.setNombres(p.getNombres());
            car.setDescripcion(p.getDescripcion());
            car.setPrecioCompra(p.getPrecio());
            car.setCantidad(cantidad);
            car.setSubTotal(cantidad*p.getPrecio());
            listaCarrito.add(car);
            if (car != null) {
//                request.setAttribute("contador", listaCarrito.size());
//                response.sendRedirect("/CarritoConFetch");
               String resultado = convertir.toJson(car);
               out.println("" + resultado);
            }

        } else {    
        //This variable stores all the rows in the table.
        ArrayList<producto> listado = productodao.Listado();
        //It evaluates if the variable is not null. 
        if (listado != null) {
            //The whole table is converted to JSON and stored in a variable.
            String resultado = convertir.toJson(listado);
            //We send the result to the client
            out.println("" + resultado);
        }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
