package modelo;

import configuracion.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.TreeMap;


public class productodao extends conexion{
    public static ArrayList<producto> Listado(){
        
        ArrayList<producto> productos=new ArrayList<>();
        Connection con;
        
        try {
            con = conexion.getConnection();
            PreparedStatement consulta=con.prepareStatement("Select * from producto");
            ResultSet resultado = consulta.executeQuery();
            while(resultado.next()){
                producto prod=new producto();
                prod.setId(resultado.getInt("idProducto"));
                prod.setNombres(resultado.getString("nombre"));
                prod.setFoto(resultado.getString("foto"));
                prod.setDescripcion(resultado.getString("descripcion"));
                prod.setPrecio(resultado.getDouble("precio"));
                prod.setStock(resultado.getInt("stock"));
                productos.add(prod);
            }
            resultado.close();
            consulta.close();
            con.close();
        } catch (Exception e) {
           System.err.println("SQLException: " + e.getMessage());
           return null;
        }
        
        
        return productos;
    }
    
    public static producto BuscarXindice(int id){
    
            producto prod=new producto();
            Connection con;
            
        try {
            //We obtain the connection through the method and assign it to a variable.
            con = conexion.getConnection();
            //It prepares the query and stores it to a variable of type PrepareStatement.
            PreparedStatement sentencia = con.prepareStatement("Select * from producto where idProducto =" + id);
            //It executes the query and stores it in a variable called result.
            ResultSet resultado = sentencia.executeQuery();
            //Condition used to ask if there is more data in the table
            //so The next method moves the cursor to the next row.
            while(resultado.next()){
                
                prod.setId(resultado.getInt("idProducto"));
                prod.setNombres(resultado.getString("nombre"));
                prod.setFoto(resultado.getString("foto"));
                prod.setDescripcion(resultado.getString("descripcion"));
                prod.setPrecio(resultado.getDouble("precio"));
                prod.setStock(resultado.getInt("stock"));
            }
            //Execute the query and then close the connection.
            resultado.close();
            sentencia.close();
            con.close();
            //Connection error.
        } catch (Exception ex) {
            System.err.println("SQLException: " + ex.getMessage());
           return null;
        }
        //It contains and returns the specific row from table 'autos'.
         return prod;
    }
    
}
